/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function userInfo(){
	let fullName = prompt("Last Name, Firt Name");
	let age = prompt("Enter your age: ");
	let location = prompt("Enter Address");
	console.log("Hi: " + fullName);
	console.log(fullName +" age is"+ age);
	console.log(fullName +" is located at " + location);
};
userInfo();



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/function showTopArtist(){
	let topArtist = ["Twenty One Pilots", "Eminem", "Queen", "Nirvana", "All time Low"];
	console.log(topArtist);
};
showTopArtist();

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function showtopMovies(){
		let topOneMovie = "Memento: ";
		let mementoTomato = "93%";
		console.log("1. "+topOneMovie);
		console.log("TomatoMeter for" + topOneMovie + ": " +mementoTomato);

		let topTwoMovie ="London Has fallen";
		let londonTomato ="27%";
		console.log("2." + topTwoMovie);
		console.log("TomatoMeter for" + topTwoMovie+": " + londonTomato);

		let topThreeMovie = "Dear John";
		let djTomato ="28%";
		console.log("3." + topThreeMovie);
		console.log("TomatoMeter for " + topThreeMovie +": "+ djTomato);

		let topFourMovie = "The Lucky One";
		let lOneTomato ="";
		console.log("4." + topFourMovie);
		console.log("TomatoMeter for : " + topFourMovie+": "+lOneTomato);

		let topFiveMovie ="Shawn Hank Redemption";
		let hankTomato ="91%";
		console.log("5." +topFiveMovie);
		console.log("TomatoMeter for : " + topFiveMovie+": " + hankTomato);
 


	};
	showtopMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


	function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();
